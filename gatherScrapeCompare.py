#!/usr/bin/env python

from __future__ import print_function
from bs4 import BeautifulSoup
import urllib2
from socket import error as socketerror
import errno
import subprocess
from time import sleep
from os import system
import re
import sys

# Initializations
objects = []
major_missing = []
minor_missing = []


def getbox(url):
    try:
        content = urllib2.urlopen(url).read()
    except socketerror as e:
        if e.errno != errno.ECONNRESET:
            raise  # Not error we are looking for
        print("connection reset trying again...")
        content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    sleep(1)  # sleep so the host doesn't kick us
    for link in soup.find_all('tr'):
        text = str(link.contents[0])
        link = link.get('td')
        print(link)


def getlink(url):
    """
    takes a base link like "www.mirrors.kernel.org/centos" and returns the links of any suitable install location recursively
    """ 
    global objects
    try:
        content = urllib2.urlopen(url).read()
    except (socketerror, urllib2.HTTPError) as e:
        if e.errno == errno.ECONNRESET:
            print("connection reset trying again...")
            content = urllib2.urlopen(url).read()
        else:
            return
    soup = BeautifulSoup(content)
    print("Crawling "+url+" for .iso files...")
    sleep(1)  # sleep so the host doesn't kick us

    for link in soup.find_all('a'):
        text = str(link.contents[0])
        link = link.get('href')
        if link is None:
            return
        if "http" not in link and link != "/" and "=" not in link \
                and text != "Parent Directory" and link != "docs/" and link != "SRPMS/" and link.endswith("/")\
                and link != "boot/" and link != "i386/" and link != ".." and link != "../" and link != "bt-cd/"\
                and link[:-1] != url.split("/")[-2] and link != "virtio-win/":
                link.rstrip()
                if url.endswith("/"):
                    getlink(url+link)
                else:
                    getlink(url+"/"+link)

        elif link.endswith(".iso") and "netinst" in link:
            objects.append(url+link)
            print("ISO Found, Name: "+url+link+"\r")
        elif link.endswith(".iso") and "NET" in link:
            objects.append(url+link)
            print("ISO Found, Name: "+url+link+"\r")
        elif link.endswith(".iso") and "debian" in link and "standard" in link:
            objects.append(url+link)
            print("ISO Found, Name: "+url+link+"\r")

    return objects


def getubuntulink(url):
    """
    Looks through https://wiki.ubuntu.com/Releases to see what the most recent releases are and returns a fake url to the iso
    """
    global objects
    try:
        content = urllib2.urlopen(url).read()
    except (socketerror, urllib2.HTTPError) as e:
        if e.errno == errno.ECONNRESET:
            raise  # Not error we are looking for
        print("connection reset trying again...")
        content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content)
    print("Checking "+url+" for new ubuntu dists...")
    sleep(1)  # sleep so the host doesn't kick us

    for link in soup.find_all('tr'):
        text = str(link.contents)
        if "<p class=\"line862\">Ubuntu" in text and "LTS" not in text:
            text = text.split('<p class=\"line862\">Ubuntu ', 1)[-1]
            text = text.split('<', 1)[0]
            text = "fakeurl/Ubuntu-"+text+"-x86_64-netinstall.iso"
            objects.append(text)
            print("Fake URL created "+text)
    return objects




def getnames():
    """
    Returns a formatted list of the machines we have on sparky $variant$major-$minor
    """
    output = subprocess.check_output(['sudo', 'virsh', 'list', '--all'], shell=False)
    output = output.split()
    names = []
    for name in output:
        if "automationTest" in name:
            name = name.replace('automationTest_', '')
            name.split("-")
            names.append(name)
    return names


def download(missing):
    """
    #Creates a VM from the iso link
    """
    hostname = missing.split("/")[-1]
    hostname = hostname.replace("-", "", 1)
    hostname = hostname.replace(".", "-", 1)
    hostname = hostname.split("-")[0] + "-" + hostname.split("-")[1]
    hostname = hostname.lower()
    hostname = "automationTest_"+hostname

    location = '/home/nsadmin/packer_cache/'+missing.split("/")[-1]
    print(location)

    subprocess.check_call(['sudo', 'wget', '-O', location, missing])

    variant = "generic"
    kickstart = ""
    if "centos" in hostname.lower():
        if "centos5" in hostname.lower():
            variant = "rhel5"
            kickstart = "centos5-ks.cfg"
        if "centos6" in hostname.lower():
            variant = "rhel6"
            kickstart = "centos6-ks.cfg"
        if "centos7" in hostname.lower():
            variant = "rhel7"
            kickstart = "centos7-ks.cfg"
    # elif Add debian, ubuntu, etc.

    print("IN DOWNLOAD WITH: "+missing+" HOSTNAME: "+hostname)
    subprocess.check_call(['sudo', 'qemu-img', 'create', '-f', 'qcow2', '/var/lib/libvirt/images/'+hostname+'.qcow2',
                           '8192'], shell=False)
    subprocess.check_call(['sudo', 'virt-install', '--name', hostname, '--ram', '2048', '--disk',
                                    'path=/var/lib/libvirt/images/'+hostname+'.qcow2,size=12', '--vcpus', '1',
                                    '--os-type', 'linux', '--os-variant', variant, '--network', 'bridge=br0',
                                    '--graphics', 'none', '--console', 'pty,target_type=serial',
                                    '--location', "\'"+location+"\'", '--initrd-inject=/home/nsadmin/kickstarts/'+kickstart,
                                    '--extra-args', '\'ks=file:/'+kickstart+' console=ttyS0,115200n8 serial\''],
                                    shell=False)
    print("VM is installing and will be up in some time")
    add_etchosts(hostname)


def add_etchosts(hostname):
    """
    Add hostname and corresponding IP to /etc/hosts
    """
    output = subprocess.check_output(('sudo', 'virsh', 'dumpxml', hostname), shell=False)
    output = output.split('\n')
    for line in output:
        if "mac address" in line:
            mac = line
            break
    mac = mac.split("'")[1]

    output2 = subprocess.check_output(['arp', '-a'], shell=False)
    output2 = output2.split('\n')
    for line in output2:
        if mac in line:
            ip = line
            break
    ip = ip.split("(")[-1]
    ip = ip.split(")")[0]

    with open('/etc/hosts', 'rt') as f:
        s = f.read() + ip + '\t\t\t' + hostname + '\n'
    with open('/tmp/etc_hosts.tmp', 'wt') as outf:
        outf.write(s)

    subprocess.check_output(('sudo', 'mv', '/tmp/etc_hosts.tmp', '/etc/hosts'))


def hasnumbers(inputstring):
    return any(char.isdigit() for char in inputstring)


def hipchat(name, type, location):
    # TODO change notify to true
    if type == "minor":
        system("curl -d '{\"color\":\"yellow\",\"message\":\"New "+type+" version "+name+" found at: "+location+"\",\"notify\":false,\"message_format\":\"text\"}' -H 'Content-Type: application/json' https://ivc-inc.hipchat.com/v2/room/2374097/notification?auth_token=7oHdtVz6LJ8qkborN8oxJtvXa2SOi1gt6X2XoBuJ")
    else:
        system("curl -d '{\"color\":\"red\",\"message\":\"New "+type+" version "+name+" found at: "+location+"\",\"notify\":true,\"message_format\":\"text\"}' -H 'Content-Type: application/json' https://ivc-inc.hipchat.com/v2/room/2374097/notification?auth_token=7oHdtVz6LJ8qkborN8oxJtvXa2SOi1gt6X2XoBuJ")


def main(url):
    """
    Takes the links from getlink() and the names from getnames() and returns the links that we should download
    """
    if args[1] == "ubuntu":
        links = getubuntulink(url)
    else:
        links = getlink(url)
    names = getnames()
    formatted_links = list(links)
    major_names = list(names)

    # Format links to match names
    for i in xrange(len(formatted_links)):
        if "-live-" in formatted_links[i]:
            formatted_links[i] = formatted_links[i].replace("-live", "")
        formatted_links[i] = formatted_links[i].split("/")[-1]

        formatted_links[i] = formatted_links[i].replace("-", "", 1)
        formatted_links[i] = formatted_links[i].replace(".", "-", 1)
        if args[1] == "ubuntu":
            formatted_links[i] = formatted_links[i].split("-")[0] + formatted_links[i].split("-")[1]
        else:
            formatted_links[i] = formatted_links[i].split("-")[0] + "-" + formatted_links[i].split("-")[1]
        formatted_links[i] = formatted_links[i].lower()
        if "x86_64" in formatted_links[i]:
            formatted_links[i] = formatted_links[i].replace("x86_64", "")
        if ".0" in formatted_links[i]:
            formatted_links[i] = formatted_links[i].replace(".0", "", 1)


    # Format names to get major versions and find most recent major number
    centos_major_number = 0
    debian_major_number = 0
    fedora_major_number = 0
    opensuse_major_number = 0
    ubuntu_major_number = 0
    for i in xrange(len(major_names)):
        major_names[i] = major_names[i].split("-")[0]
        if "centos" in major_names[i]:
            tmp = re.sub("[^0-9]", "", major_names[i])
            if centos_major_number < tmp:
                centos_major_number = int(tmp)
        elif "debian" in major_names[i]:
            tmp = re.sub("[^0-9]", "", major_names[i])
            if debian_major_number < tmp:
                debian_major_number = int(tmp)
        elif "fedora" in major_names[i]:
            tmp = re.sub("[^0-9]", "", major_names[i])
            if fedora_major_number < tmp:
                fedora_major_number = int(tmp)
        elif "opensuse" in major_names[i]:
            tmp = re.sub("[^0-9]", "", major_names[i])
            if opensuse_major_number < tmp:
                opensuse_major_number = int(tmp)
        elif "ubuntu" in major_names[i]:
            tmp = re.sub("[^0-9]", "", major_names[i])
            if ubuntu_major_number < tmp:
                ubuntu_major_number = int(tmp)

    # compare iso to vms in environment
    has_maj = False
    has_exact = False
    for i in xrange(len(formatted_links)):
        if "ubuntu" in formatted_links[i]:
                formatted_links[i] = formatted_links[i].strip()
                tmp = formatted_links[i][-2:]
                formatted_links[i] = formatted_links[i][:-2]
                formatted_links[i] = formatted_links[i]+"-"+tmp

        for j in xrange(len(names)):
            if (formatted_links[i].split("-")[0] == major_names[j]) and (has_maj == False) and (has_exact == False):
                has_maj = True
            if (formatted_links[i] == names[j]) and (has_exact == False):
                print("We already have the version "+formatted_links[i])
                has_exact = True

        # New minor version.
        if has_maj == True and has_exact == False:
            print("There's a new minor version available at "+links[i])
            if "ubuntu" in links[i]:
                hipchat(formatted_links[i], "minor", "http://wiki.ubuntu.com/Releases")
            elif "x86" in links[i]:
                hipchat(formatted_links[i], "minor", links[i])
        # New major version that is more than 3 major versions old
        if has_maj == False and has_exact == False:
            print("There's a new major version available at "+links[i])
            found_maj_number = formatted_links[i].split('-')[0]
            found_maj_number = re.sub("[^0-9]", "", found_maj_number)
            found_maj_number = float(found_maj_number)
            if "centos" in formatted_links[i]:
                if found_maj_number >= (centos_major_number - 2) and "x86" in links[i]:
                    hipchat(formatted_links[i], "major", links[i])
            elif "debian" in formatted_links[i]:
                if found_maj_number >= (debian_major_number - 2):
                    hipchat(formatted_links[i], "major", links[i])
            elif "fedora" in formatted_links[i]:
                if found_maj_number >= (fedora_major_number - 2) and "x86" in links[i]:
                    hipchat(formatted_links[i], "major", links[i])
            elif "opensuse" in formatted_links[i]:
                if found_maj_number >= (opensuse_major_number - 2) and "x86" in links[i]:
                    hipchat(formatted_links[i], "major", links[i])
            elif "ubuntu" in formatted_links[i]:
                print("ubuntu major number: "+str(ubuntu_major_number))
                if found_maj_number >= (ubuntu_major_number - 2):
                    hipchat(formatted_links[i], "major", "http://wiki.ubuntu.com/Releases")

        # Reset flags
        has_maj = False
        has_exact = False

args = sys.argv

# Sort out weirdness of ubuntu trying to run before others
if "ubuntu" in args:
    args.insert(len(args)+1, args.pop(args.index("ubuntu")))

args_flag = False

if len(args) == 1:
    print("Please specify type of distro to scrape for example: gsc centos\n"
          "Available options: debian, centos, fedora, ubuntu, opensuse")
else:
    if "debian" in args:
        main("http://cdimage.debian.org/debian-cd/")
        args_flag = True
    if "centos" in args:
        main("http://vault.centos.org/")
        args_flag = True
    if "fedora" in args:
        main("http://archives.fedoraproject.org/pub/")
        args_flag = True
    if "ubuntu" in args:
        main("http://wiki.ubuntu.com/Releases")
        args_flag = True
    if "opensuse" in args:
        main("http://mirrors.kernel.org/opensuse/")
        args_flag = True
    elif args_flag == False:
        print("Please specify type of distro to scrape for example: gsc centos\n"
            "Available options: debian, centos, fedora, ubuntu, opensuse")





